from numpy import random
from hashlib import sha256
import seaborn as sns; sns.set_theme()
import numpy as np
import matplotlib.pyplot as plt
import math
import sys
from progress.bar import IncrementalBar


h = "minchiàziò"


def hashing(block_hash):
    hashcode = sha256(block_hash.encode()).hexdigest()
    return hashcode
"""
def generate_image(block_hash):
    cmaps = ['Accent', 'Accent_r', 'Blues', 'Blues_r', 'BrBG', 'BrBG_r', 'BuGn', 'BuGn_r', 'BuPu', 'BuPu_r', 'CMRmap', 'CMRmap_r', 'Dark2', 'Dark2_r', 'GnBu', 'GnBu_r', 'Greens', 'Greens_r', 'Greys', 'Greys_r', 'OrRd', 'OrRd_r', 'Oranges', 'Oranges_r', 'PRGn', 'PRGn_r', 'Paired', 'Paired_r', 'Pastel1', 'Pastel1_r', 'Pastel2', 'Pastel2_r', 'PiYG', 'PiYG_r', 'PuBu', 'PuBuGn', 'PuBuGn_r', 'PuBu_r', 'PuOr', 'PuOr_r', 'PuRd', 'PuRd_r', 'Purples', 'Purples_r', 'RdBu', 'RdBu_r', 'RdGy', 'RdGy_r', 'RdPu', 'RdPu_r', 'RdYlBu', 'RdYlBu_r', 'RdYlGn', 'RdYlGn_r', 'Reds', 'Reds_r', 'Set1', 'Set1_r', 'Set2', 'Set2_r', 'Set3', 'Set3_r', 'Spectral', 'Spectral_r', 'Wistia', 'Wistia_r', 'YlGn', 'YlGnBu', 'YlGnBu_r', 'YlGn_r', 'YlOrBr', 'YlOrBr_r', 'YlOrRd', 'YlOrRd_r', 'afmhot', 'afmhot_r', 'autumn', 'autumn_r', 'binary', 'binary_r', 'bone', 'bone_r', 'brg', 'brg_r', 'bwr', 'bwr_r', 'cividis', 'cividis_r', 'cool', 'cool_r', 'coolwarm', 'coolwarm_r', 'copper', 'copper_r', 'cubehelix', 'cubehelix_r', 'flag', 'flag_r', 'gist_earth', 'gist_earth_r', 'gist_gray', 'gist_gray_r', 'gist_heat', 'gist_heat_r', 'gist_ncar', 'gist_ncar_r', 'gist_rainbow', 'gist_rainbow_r', 'gist_stern', 'gist_stern_r', 'gist_yarg', 'gist_yarg_r', 'gnuplot', 'gnuplot2', 'gnuplot2_r', 'gnuplot_r', 'gray', 'gray_r', 'hot', 'hot_r', 'hsv', 'hsv_r', 'inferno', 'inferno_r', 'jet', 'jet_r', 'magma', 'magma_r', 'nipy_spectral', 'nipy_spectral_r', 'ocean', 'ocean_r', 'pink', 'pink_r', 'plasma', 'plasma_r', 'prism', 'prism_r', 'rainbow', 'rainbow_r', 'seismic', 'seismic_r', 'spring', 'spring_r', 'summer', 'summer_r', 'tab10', 'tab10_r', 'tab20', 'tab20_r', 'tab20b', 'tab20b_r', 'tab20c', 'tab20c_r', 'terrain', 'terrain_r', 'turbo', 'turbo_r', 'twilight', 'twilight_r', 'twilight_shifted', 'twilight_shifted_r', 'viridis', 'viridis_r', 'winter', 'winter_r']

    hashcode = hashing(block_hash)

    hash_int = int(str(hashcode), base=16)
    hash_int = str(hash_int)
    print(hash_int)
    x = hash_int[0] + hash_int[1] + hash_int[2]
    x = int(x) 
    y = x
    code = random.random((x,y))
    imsave('image6.png',code, cmap=get_cmap(cmaps[int(hash_int[3] + hash_int[4])]))
    
    return True
"""

def generate_heatmap(block_hash):
    hashcode = int(hashing(block_hash), base=16)

    progressbar = IncrementalBar('Processing BlockImage',suffix='%(percent)d%%')

    w = 1920        
    h = 1080     
    e = 0
    divider = len(str(hashcode))+1
    
    cX = ((float('7'+str(hashcode)))/math.pow(10,divider))*-1
    print(cX)
    cY = int(str(hashcode)[-1])/10
    moveX, moveY = 0.0, 0.0
    maxIter = 255
    map = []
    p2 = 0
    print('\n')    
    def fib(n):
        counter = 0

        first = 1
        second = n
        temp = 0
        result = 0
 
        while counter <= 10:
            temp = first + second
            first = second
            second = temp
            counter = counter + 1
            result +=first
            return result

    for x in range(w):
        
        array = []
        for y in range(h):
            
            zx = 1+(x- w/2)/(0.5*1*w) + moveX
            zy = (y- h/2)/(0.5*1*h) + moveY

            i = maxIter
            while zx*zx + zy*1*zy < 4 and i > 1:
                tmp = zx*zx - zy*(x/((int(str(hashcode)[4])+1)*100))*zy + cX
                zy,zx = 2.0*zx*zy + cY, tmp
                i -= 1
            array.insert(y,i)
        map.insert(x,array)
        p = int((x/w)*100)
        
        if (p == (p2+1)):
            progressbar.next()
            p2 = p
    progressbar.next()
    progressbar.finish()
            

    plt.grid(False)
    plt.axis('off')
    plt.pcolormesh(map).set_animated(True)


    filename = str(hashcode) + '.png'
    path = 'block_images/' + filename
    plt.savefig(path, dpi=600)
    

    
    return True
