from numba import jit, cuda
from flask import Flask, request


from blockchain import *

import random
import time


app = Flask(__name__)
blockchain = Blockchain()

@app.route('/chain', methods=['GET', 'POST'])
def get_chain():
    chain_data = []
    for block in blockchain.chain:
        chain_data.append(block.__dict__)
    return json.dumps({"length": len(chain_data),
                       "chain": chain_data})

@jit(target_backend='cuda')                         
@app.route('/chain/mine', methods=['GET', 'POST'])
def mining():
    chain_data = []
    while True:    
        start_time = time.time()
        new_transaction = [random.random(),random.random()]
        blockchain.add_new_transaction(new_transaction)
        blockchain.mine()
        total_time = round((time.time()-start_time),2)
        print('\n\nMining time',total_time, 's')
        print('BlockImage saved!\n')

        for block in blockchain.chain:
                chain_data.append(block.__dict__)
        
if __name__ == '__main__':
    app.run(debug=True, port=5000)